a, b, c = map(int, input(
    "Введите a, b, c через пробел: "
).split())

d = b ** 2 - 4 * a * c
if d < 0:
    print("Корней нет")
elif d > 0:
    sd = d ** 0.5
    x1 = (-b + sd) / (2 * a)
    x2 = (-b - sd) / (2 * a)
    print(f"x1 = {x1}")
    print(f"x2 = {x2}")
else:
    x = -b / (2 * a)
    print(f"x = {x}")